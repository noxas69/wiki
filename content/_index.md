+++
title = "Wiki noxas"


# The homepage contents
[extra]
lead = 'Documentation - cheatsheet utile pour le dev et le sysadmin'
repo_license = "Open-source MIT License."
url = "/docs/getting-started/introduction/"
url_button = "Par ici..."

# Menu items
[[extra.menu.main]]
name = "Docs"
section = "docs"
url = "/docs/getting-started/introduction/"
weight = 10

[[extra.menu.main]]
name = "Blog"
section = "blog"
url = "/blog/"
weight = 20

[[extra.list]]
title = ""
content =""

+++
