+++
title = "Cheatsheet"
date = 2021-05-01T18:20:00+00:00
updated = 2021-05-01T18:20:00+00:00
draft = false
weight = 420
sort_by = "weight"
template = "docs/page.html"

[extra]
toc = true
top = false
+++


kubectl basic

```bash
# infos du cluster
kubectl cluster-info

# lister les nodes
kubectl get nodes

# lister les nodes avec leurs IP
kubectl get nodes -o wide

# lister tous les objets du cluster (pod, déploiement, réplica, service...)
kubectl get all

# éditer un objet
kubectl edit RESSOURCE

# Créer un pod
kubectl apply -f POD_SPEC.yaml

# lister les infos d'un pods pour avoir les container lancer dedans, les images etc
kubectl describe pods

# lancer une commande dans un container
kubectl exec POD_NAME -c CONTAINER_NAME -- env

# Se connecter au container
kubectl exec -ti POD_NAME -c CONTAINER_NAME -- bash

# Log d'un container d'un pod
kubectl logs POD_NAME -c CONTAINER_NAME

# Forward de port
kubectl pord-forward POD_NAME HOST_PORT:CONTAINER_PORT
kubectl pord-forward web 8080:80

# Ajouter un label à un node
kubectl label nodes NODE_NAME disktype=ssd

# Créer une configmap à partir d'un fichier de conf
kubectl create configmap www-config --from-file=./nginx.conf

# get all verbs and associated ressource
kubectl api-resources --sort-by name -o wide
```


## debug command

```bash
# check tout ce qui a sur le cluster
kubectl get all

# pour toute les commande on peut spécifier le namespace
kubectl get all -n $NAMESPACE

#check les évents d'un namespace
kubectl get events -n demo

#check les events et erreur d'un pod
kubectl describe pods $POD

#check log pod
kubectl logs $POD

#check les events
kubectl get events -n $NAMESPACE

#check le fichier de conf yaml d'une ressource
kubectl get pods $POD  -o yaml

```

Pour éviter de taper les yaml à la mano, il peux être pratique de les générer via la commande kubectl

```
# yaml de création d'un pod 
kubectl run nginx --image=nginx --dry-run=client -o yaml

# yaml deployment
kubectl create deployment --image=nginx nginx --dry-run=client -o yaml
```

##### type de services :

**Nodeport** : permet d'exposer un pod à l'extérieur du cluster (pour accèder un site web depuis l'extérieur par exemple)  
**ClusterIP** : permet d'exposer un pod à l'intérieur du cluster (pour faire communiquer les pod, exemple un serveur web vers un serveur bdd)  
**LoadBalancer** : dispo uniquement sur les cloud provider pour répartir la charge

##### Exemple de spécification

---

Fichier yaml pour créer un pod simple d'une appli wordpress avec sa BDD :

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: wp
spec:  
  containers:
  - image: wordpress:4.9-apache
    name: wordpress
    env:
    - name: WORDPRESS_DB_PASSWORD
      value: mysqlpwd
    - name: WORDPRESS_DB_HOST
      value: 127.0.0.1
  - image: mysql:5.7
    name: mysql
    env:
    - name: MYSQL_ROOT_PASSWORD
      value: mysqlpwd
    volumeMounts:
    - name: data
      mountPath: /var/lib/mysql                                                                                                                              
  volumes:
  - name: data
    emptyDir: {}

```
