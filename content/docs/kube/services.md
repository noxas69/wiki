+++
title = "services"
date = 2021-05-01T18:20:00+00:00
updated = 2021-05-01T18:20:00+00:00
draft = false
weight = 420
sort_by = "weight"
template = "docs/page.html"

[extra]
toc = true
top = false
+++

Liste utile des services permettant de faire fonctionner un cluster kube

##### HorizontalPodAutoscaler

Permet de scale automatiquement des pods. On définit un min et un max et par exemple si le CPU dépasse 60% d'utilisation un pods sera créé automatiquement :

```yaml
apiVersion: autoscaling/v1
kind: HorizontalPodAutoscaler
metadata:
  name: front-apache
  namespace: clientxxx
spec:
  maxReplicas: 8
  minReplicas: 2
  scaleTargetRef:
    apiVersion: apps/v1
    kind: Deployment
    name: front-apache
  targetCPUUtilizationPercentage: 60
```

##### PersistantVolume et PersistentVolumeClaim

Les données dans un conteneur sont éphémères. Lorsqu'un conteneur atteint sa fin de vie, ses fichiers sont effacés. Il est parfois nécessaire de sauvegarder des données afin qu'elles restent disponibles même après l'arrêt du conteneur.

> Un `PersistentVolume` (PV) est un élément de stockage dans le cluster qui a été provisionné par un administrateur ou provisionné dynamiquement. Il s’agit d’une ressource dans le cluster, tout comme un nœud est une ressource de cluster. Ils ont un cycle de vie indépendant de tout pod.

> Un `PersistentVolumeClaim` (PVC) est une demande de stockage réalisée par un utilisateur ( Claim ) car comme à l'image d'un pod qui consomme des ressources de noeud, il peut également demander des niveaux spécifiques de ressources ( CPU/mémoire ). Le PVC consomme des ressources PV et peut exiger une taille particulière, des modes d'accès spécifiques ( Lecture seule, lecture/ecriture, etc ).

en gros on créer un PV et une fois fait, on créer des demande de volume sur ce pv (PVC). Dans cet exemple on créer un PV de 1Go, et ensuite on demande 500M dans le PVC

```yaml
kind: PersistentVolume
apiVersion: v1
metadata:
   name: access-log-pv
spec:
  capacity:
    storage: 1Gi
  accessModes:
    - ReadWriteOnce
  hostPath:
    path: /var/log/traefik
    type: DirectoryOrCreate
```

```yaml
kind: PersistentVolumeClaim
apiVersion: v1
metadata:
   name: access-log-pvc
spec:
   accessModes:
      - ReadWriteOnce
   resources:
      requests:
        storage: 500Mi
```

ensuite on peux utiliser ce volume pour nos déploiement :

<details id="bkmrk-kind%3A-deployment-api"><summary></summary>

```yaml
kind: Deployment
apiVersion: apps/v1
metadata:
  namespace: default
  name: traefik
  labels:
    app: traefik

spec:
  replicas: 1
  selector:
    matchLabels:
      app: traefik
  template:
    metadata:
      labels:
        app: traefik
    spec:
      serviceAccountName: traefik-ingress-controller
      containers:
        - name: traefik
          image: traefik:v2.2
          volumeMounts:
            - name: access-log-pvc 
              mountPath: "/letsencrypt/"
      volumes:
        - name: access-log-pvc
          persistentVolumeClaim:
             claimName: access-log-pvc
```

</details>mode d'accès des volumes :

- ReadWriteOnce -- le volume peut être monté en lecture-écriture par un seul nœud
- ReadOnlyMany -- le volume peut être monté en lecture seule par plusieurs nœuds
- ReadWriteMany -- le volume peut être monté en lecture-écriture par de nombreux nœuds

##### ConfigMap

Les ConfigMaps sont un moyen de stocker des informations de configuration dans Kubernetes. Ils permettent de stocker des variables d'environnement, des fichiers de configuration et des chaînes de connexion de base de données qui peuvent être utilisées par les conteneurs dans un pod Kubernetes. Les ConfigMaps peuvent être créées à l'aide de la commande `kubectl create configmap` ou à l'aide d'un générateur ConfigMap dans un fichier `kustomization.yaml`.

C'est très utile car ça permet d'exporter la configuration hors des pods et de rendre les pods portable. Par exemple pour un pod nginx on peut créer une configmap qui contient la conf du serveur nginx. On bind ensuite cette configmap dans le pod. Exemple :

<details id="bkmrk-apiversion%3A-apps%2Fv1-"><summary></summary>

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: www
spec:
  selector:
    matchLabels:
      app: www
  template:
    metadata:
      labels:
        app: www
    spec:
      containers:
      - name: nginx
        image: nginx:1.14-alpine
        volumeMounts:
        - name: config
          mountPath: "/etc/nginx/"
      volumes:
      - name: config
        configMap:
          name: www-config

```

</details>
