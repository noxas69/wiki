+++
title = "tmux"
date = 2021-05-01T18:20:00+00:00
updated = 2021-05-01T18:20:00+00:00
draft = false
weight = 420
sort_by = "weight"
template = "docs/page.html"

[extra]
toc = true
top = false
+++



**\<prefix> c** : Créer une fenêtre  
**\<prefix> p ou n** : switch de fenêtre  
**\<prefix> &** : fermer une fenêtre  
**\<prefix> w** : liste des fenêtre
**\<prefix> ,** : renommer une fenêtre
**\<prefix> "** : créer un pane verticale  
**\<prefix> %** : créer un pane horitontal  
**\<prefix> z** : zoomer / dezoomer sur un pane  
**\<prefix> x** : fermer un pane


