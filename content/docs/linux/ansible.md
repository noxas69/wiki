+++
title = "Ansible"
date = 2021-05-01T18:20:00+00:00
updated = 2021-05-01T18:20:00+00:00
draft = false
weight = 420
sort_by = "weight"
template = "docs/page.html"

[extra]
toc = true
top = false
+++

**Executer commande shell sur host distant**

```shell
ansible nom_group_host -a "mkdir /tmp/test"
```

#### Les conditions

uniquement si le host est dans le groupe :

```yaml
{% if "prod" in group_names %}
	# do some things
{% endif %}
```

Uniquement si le host est égal à :

```yaml
{% if inventory_hostname ==  "server1" %}
     # do some things
{% endif %}
```

#### Rendre les commande shell indempotent (module command ou shell)

```yaml
- name: Put docker package on hold
  shell: >
         apt-mark showholds | grep -q docker-ce
         && echo -n HOLDED
         || apt-mark hold docker-ce
  register: docker_hold
  changed_when: docker_hold.stdout != 'HOLDED'
```