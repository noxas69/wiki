+++
title = "Bash script"
date = 2021-05-01T18:20:00+00:00
updated = 2021-05-01T18:20:00+00:00
draft = false
weight = 420
sort_by = "weight"
template = "docs/page.html"

[extra]
toc = true
top = false
+++



#### base d'un script avec bonne pratique

```bash
#!/usr/bin/env bash

set -euo pipefail

# for debug :
# set -x

```

#### Variable

Affectation et utilisation d'une variable

```shell
maVariable="test"
monAutreVariable=2

echo $maVariable
echo ${maVariable}
```

On peux stocker le résultat d'une commande dans une variable

```shell
nb=$(ls -l /etc | wc -l)

echo $nb
50
```

#### Variables spéciales

Il existe quelques variables spéciales :  
  
**$$** : PID du shell courant  
**$!** : PID du dernier travail lancé en arrière plan  
**$?** : code retour de la dernière commande  
**$1** : premier argument du script  
**$2** : deuxième argument  
**$(@)** : tous les arguments  
Et d'autres variables prédéfinies, dites variables d'environnement :  
  
**$HOME** chemin du répertoire personnel de l'utilisateur  
**$OLDPWD** chemin du répertoire précédent  
**$PATH** liste des chemins de recherche des commandes exécutables  
**$PPID** PID du processus père du shell  
**$PS1** invite principale du shell  
**$PWD** chemin du répertoire courant  
**$RANDOM** nombre entier aléatoire compris entre 0 et 32767  
**$REPLY** variable par défaut de la commande "read" et de la structure shell "select"  
**$SECONDS** nombre de secondes écoulées depuis le lancement du shell

#### Les conditions 

```shell
if [[ condition ]]
then
    commande 1
    commande 2
fi
```

**comparaison d'entier**

**-eq** : est égal à  
**-ne** : n'est pas égal à  
**-gt** : est plus grand que  
**-ge** : est plus grand ou égal à  
**-lt** : est plus petit que  
**-le** : est plus petit ou égal à

exemple

```shell
if [[ $a -eq $b ]]
```

**Test sur les fichiers**

**-e file** : Vrai si le fichier existe  
**-d file** : Vrai si le fichier existe et que c'est un répertoire  
**-f file** : Vrai si le fichier existe et que c'est un fichier ordinaire  
**-h file** : Vrai si le fichier existe et que c'est un lien symbolique  
**-r file** : Vrai si le fichier possède les droits de lecture  
**-s file** : Vrai si le fichier a une taille non nulle  
**-w file** : Vrai si le fichier possède les droits d'écriture  
**-x file** : Vrai si le fichier possède les droits d'exécution  
**-z** : Vrai si la chaine est non vide

exemple

```shell
if [[ -e /tmp/fichier ]]
```

#### Boucle for

```shell
for element in Hydrogen Helium Lithium Beryllium
do
  echo "Element: $element"
done
```

```shell
for i in $(seq 1 5)
do
    echo $i
    commande1
    commande2
    commandeN
done
```

#### Boucle while

```shell
i=0

while [ $i -le 2 ]
do
  echo Number: $i
  ((i++))
done
```

#### Autres Astuces

On veux sortir des nombre avec un format particulier, par exemple toujours 4 chiffres : 0001, 0002, 0100...

```bash
# pour 5 chiffres
for i in $(seq -f "%05g" 10 15)
do
  echo $i
done

```

résultat :

```
00010
00011
00012
00013
00014
00015
```

vérifier si un argument est passé à l'appel du script, sinon exit :

```bash
if [[ $# -eq 1 ]]; then
    >&2 echo "Pas de clé spécifiée"                                                                                                                          
    exit 1
fi
```

vérifier si une string est contenu dans une autre :

```bash
keys=$(cat /etc/keys.txt)
my_key="masupercle"

if [[ "$keys" == *"$my_key"*  ]]; then
	echo "la clé existe déjà"
else
```
