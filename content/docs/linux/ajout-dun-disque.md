+++
title = "Ajout d'un disque"
date = 2021-05-01T18:20:00+00:00
updated = 2021-05-01T18:20:00+00:00
draft = false
weight = 420
sort_by = "weight"
template = "docs/page.html"

[extra]
toc = true
top = false
+++

Lors de l'ajout d'un disque, identifier le avec

```shell
fdisk -l
```

puis le formater :

```shell
mkfs.ext4 /dev/sda
```

Ensuite pour le monter automatiquement au reboot modifier le fichier `/etc/fstab`

```shell
/dev/sda /backup ext4 defaults 0 0
```