+++
title = "multipass : créer des vm ubuntu hyper rapidement"
date = 2021-05-01T18:20:00+00:00
updated = 2021-05-01T18:20:00+00:00
draft = false
weight = 420
sort_by = "weight"
template = "docs/page.html"

[extra]
toc = true
top = false
+++


[https://multipass.run/install](https://multipass.run/install)

Multipass permet de lancer rapidement des VM ubuntu. Idéal pour faire des test en local (cluster kube par exemple)

Créer une VM

```bash
# par défaut la vm à 1G ram 1 cpu et 5G de disque
multipass launch --name foo

# Lancer une vm avec 2 cpu, 3G de ram et 10G de disque
multipass launch -n node2 -c 2 -m 3G -d 10G
```

Lister les VM

```bash
multipass list
```

Lancer une commande dans la vm

```bash
multipass exec node1 -- lsb_release -a

# Installer docker
multipass exec node1 -- /bin/bash -c "curl -sSL https://get.docker.com | sh"

# Se connecter à la vm
multipass shell node1
```

Monter des dossier dans la vm

```
# Création d'un fichier en local
$ mkdir /tmp/test && touch /tmp/test/hello

# Montage du répertoire dans le filesystem de la VM node1
$ multipass mount /tmp/test node1:/usr/share/test

# Vérification
$ multipass exec node1 -- ls /usr/share/test
hello

```

Copier des fichiers

```
# Copie d'un fichier depuis la machine locale
$ multipass transfer /tmp/test/hello node1:/tmp/hello

# Vérification
$ multipass exec node1 -- ls /tmp/hello
/tmp/hello

```
