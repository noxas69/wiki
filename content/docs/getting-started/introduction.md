+++
title = "Introduction"
description = "Wiki perso publique pour le sysadmin et le dev. Fait avec Zola et AdiDoks"
date = 2021-05-01T08:00:00+00:00
updated = 2021-05-01T08:00:00+00:00
draft = false
weight = 10
sort_by = "weight"
template = "docs/page.html"

[extra]
lead = 'Wiki perso publique pour le sysadmin et le dev. Fait avec Zola et AdiDoks'
toc = true
top = false
+++

